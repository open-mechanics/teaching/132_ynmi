\documentclass{YNMI_prednasky}
\usepackage{amsmath}

\newcommand{\trn}{^T}
\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\de}{d}

\begin{document}

\maketitle{1}{Okrajové úlohy}

\subsection*{Cíle}

\begin{itemize}\noitemsep
  \item Soustavy lineárních rovnic (konečná dimenze) $\rightarrow$ okrajové úlohy~(nekonečná dimenze)
  \item Dva šikovné pohledy na soustavy lineárních rovnic a jejich ekvivalence
  \item Zobecnění do nekonečné dimenze a jeho úskalí
  \item Důkaz existence řešení nekonečné úlohy pro jednu variantu pohledu 
\end{itemize}

\section{Soustavy lineárních rovnic}

\begin{itemize}\noitemsep
  \item Uvažujme soustavu lineárních rovnic z části~I předmětu~\cite[rovnice~(21)]{}
  %
  \begin{align}\label{eq:linear_system}
    \vek{A} \vek{x} = \vek{b}    
  \end{align}
  %
  kde $\vek{A} \in \mathbb{R}^{n \times n}$, $\vek{x} \in \mathbb{R}^n$ a $\vek{b} \in \mathbb{R}^n$, $\vek{A}$ je regulární matice a $n \in \mathbb{N}$~značí \emph{dimenzi} problému.

  \item Jak přepsat soustavu rovnic do \emph{jedné} rovnice pro \emph{libovolné} $n$?
  %
  \begin{itemize}
    %
    \item Varianta 1: Slabé řešení
    %
    \begin{align*}
    \vek{v}\trn \vek{A} \vek{x} 
    = 
    \vek{v}\trn \vek{b} && \forall \vek{v} \in \mathbb{R}^n
    \end{align*}

    \item Varianta 2: Variační řešení
    %
    \begin{align*}
      \vek{x} 
      = 
      \arg \min_{\vek{v} \in \mathbb{R}^n} J(\vek{v}) 
      = 
      \frac{1}{2} \vek{v}\trn \vek{A} \vek{v} 
      -  
      \vek{v}\trn \vek{b}
    \end{align*}

  \end{itemize}
\end{itemize}

\paragraph{Příklad pro $n=2$}
%
$$
\begin{bmatrix}
a_{11} & a_{12} \\
a_{21} & a_{22}
\end{bmatrix}
\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix}
=
\begin{bmatrix}
b_1 \\ b_2
\end{bmatrix}
$$

\noindent
Varianta 1 vede na
%
\begin{align*}
\begin{bmatrix}
v_1 \\ v_2
\end{bmatrix}
\trn
\begin{bmatrix}
a_{11} & a_{12} \\
a_{21} & a_{22}
\end{bmatrix}
\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix}
& =
\begin{bmatrix}
v_1 \\ v_2
\end{bmatrix}
\trn
\begin{bmatrix}
b_1 \\ b_2
\end{bmatrix}
\\
v_1 ( a_{11} x_1 + a_{12} x_2 )
+ 
v_2 ( a_{21} x_2 + a_{22} x_2 )
& =
(b_1 v_1 + b_2 v_2)
& \forall v_1, v_2 \in \mathbb{R}
\end{align*}

\noindent
Varianta 2 byla detailně diskutována v~\cite[sekce~7]{}, vyžaduje, aby matice $\vek{A}$ byla symetrická a pozitivně defintiní. Koncept slabého řešení je tedy obecnější než variačního.

\paragraph{Shrnutí}
%
\begin{itemize}
  \item Euklidův prostor $\mathbb{R}^n$
  \item Bilineární forma: $A(\vek{u}, \vek{v}) = \vek{v}\trn \vek{A}\vek{u}$, $A : \mathbb{R}^n \times \mathbb{R}^n \rightarrow \mathbb{R}$, $\vek{A}$ je symetrická pozitivně definitní matice:
  %
  \begin{align*}
    A( \vek{u}, \vek{b} ) 
    = 
    A( \vek{v}, \vek{u}), &&
    A( \vek{u}, \vek{u} ) \geq c \vek{u}\trn \vek{u}, c > 0 &&
    \text{ pro všechna } u, v \in \mathbb{R}^n 
  \end{align*}

  \item Lineární forma: $b( \vek{v} ) = \vek{v} \trn \vek{b}$, $b : \mathbb{R}^n \rightarrow \mathbb{R}$
 
  \item Slabé řešení: $A(\vek{x}, \vek{v}) = b( \vek{v} )$ pro všechna $\vek{v} \in \mathbb{R}^n$
  
  \item Variační řešení:~
  %
  $
  \vek{x} 
  = 
  \arg \min_{\vek{v} \in \mathbb{R}^n} J(\vek{v}) 
  = 
  \frac{1}{2} A(\vek{u}, \vek{u}) - b(\vek{v})
  $
 
  \item Na Eukleidové prostoru $\mathbb{R}^d$ máme k dispozici 
  %
  \begin{itemize}\noitemsep
    \item skalární součin $(\vek{u}, \vek{v})_{\mathbb{R}^n} = \vek{u}\trn \vek{v}$
    \item normu $\| \vek{u} \|_{\mathbb{R}^n}^2 = (\vek{u}, \vek{u})_{\mathbb{R}^n}$ (Pythagorova věta)
    \item Cauchyho-Schwarzovu nerovnost
    %
    \begin{align}\label{eq:C-S}
      | (\vek{u}, \vek{v})_{\mathbb{R}^n} | \leq 
      \| \vek{u} \|_{\mathbb{R}^n} \| \vek{v} \|_{\mathbb{R}^n}        
    \end{align}

    \item Konzistenci 
    %
    $
      \vek{u} \in \mathbb{R}^n 
      \Leftrightarrow 
      \| \vek{u} \|^2_{\mathbb{R}^n} < \infty
    $ 
  \end{itemize}

  \item Zobecnění Eukleidova prostoru $\mathbb{R}^n$ pro prostory funkcí $\rightarrow$ Hilbertův prostor

\end{itemize}

\section{Okrajové úlohy}
%
\begin{itemize}\noitemsep

\item Typická úloha
%
\begin{align*}
- ( a(x) u'(x) )' & = f(x)
\text{ pro } x \in (0, 1)
\\
u(0) = u(1) & = 0
\end{align*}
%
kde $a \in C^1(0, 1)$, $f \in C(0, 1)$, $u \in C^2_0(0, 1)$.

\item Fyzikální motivace: tažený/tlačený prut $a(x) = EA(x)$, $f(x)$ je
intenzita spojitého zatížení.

\item Jak zobecnit do Hilbertových prostorů?

\end{itemize}

\subsection{Lineární forma/Pravá strana~\eqref{eq:linear_system}}

\begin{itemize}\noitemsep
  \item Lineární forma: $b : C(0, 1) \rightarrow \mathbb{R}$ 
  %
  \begin{align*}
    b( v ) 
    =
    \int_0^1 v( x ) f( x ) \de x
  \end{align*}

  \item Skalární součin
  %
  \begin{align}\label{eq:scal_prod}
    (u, v)_{C(0, 1)}
    =
    \int_0^1 u( x ) v( x ) \de x    
  \end{align}
 
  \item Konzistence
  %
  \begin{align*}
    C(0, 1) 
    \neq 
    \{ 
      v \in C(0, 1);
    (v, v)_{C(0, 1)} < \infty \}
    =
    \{ 
      \int_{0}^{1} v^2(x) \de x
      < 
    \infty \}
  \end{align*}

  \item $C(0,1)$ \emph{není} Hilbertův prostor, protože není \emph{úplný}.
  
  \item Příklad: Neúplný prostor $\mathbb{Z}$
  %
  \begin{align*}
    \text{\emph{posloupnost} } \{ u_n \}_{n \in \mathbb{N}},
    &&
    u_n 
    =
    \left( 
      1 + \frac{1}{n}
    \right)^n \in \mathbb{Z},
    &&
    \lim_{n \rightarrow \infty}
    | u_n - \mathrm{e} | 
    = 
    0,
    &&
    \mathrm{e} \not\in \mathbb{Z}
  \end{align*}

  \item Úplný prostor $V$ s normou $\| \cdot \|_{V}$: každá \emph{Cauchyho} posloupnost je \emph{konvergentní}
  %
  \begin{itemize}
    
    \item Cauchyho
    %
    \begin{align*}
      \| u_m - u_n \|_{V} \rightarrow 0
    \end{align*}
  
    \item konvergentní posloupnost k $u \in V$
    %
    \begin{align*}
      \| u_n - u \|_{V} \rightarrow 0
    \end{align*}
   
  \end{itemize}

  \item Zúplněním k prostoru $V$ doplním "limity" všech Cauchyho posloupností. Například zúplněním $(\mathbb{Z}, | \cdot | )$ dostávám $\mathbb{R}$.  

  \item Zúplněním $C(0, 1)$ s normou~\eqref{eq:scal_prod} dostáváme \emph{Hilbertův} prostor
  %
  \begin{align*}
    L^2(0, 1)
    =
    \{ 
      v : (0,1) \rightarrow \mathbb{R};
      \int_{0}^{1} v^2(x) \de x
      < 
    \infty \}
  \end{align*}

  \item Zajišťuje dobře definovaný skalární součin díky Cauchy-Schwartzovy nerovnosti
  %
  \begin{align*}
    | (u, v)_{L^2(0,1)} |
    \stackrel{\eqref{eq:C-S}}{\leq}
    \| u \|_{L^2(0,1)} 
    \| v \|_{L^2(0,1)} 
    <
    \infty 
    \text{ pro všechna }
    u, v \in L^2(0,1)
  \end{align*}

\item Pro dobře definovanou lineární formu tedy stačí, aby aby $f \in L^2(0,1)$.

\end{itemize}

\subsection{Bilineární forma/Pravá strana~\eqref{eq:linear_system}}

\begin{itemize}
  \item Bilineární forma $A: C_0^2(0,1) \times C_0^2(0,1)$
  %
  \begin{align*}
    A(u, v) 
    =
    -
    \int_0^1 v(x) ( a(x) u'(x) )' \de x
    = 
    \int_0^1 v'(x) a(x) u'(x) \de x
    -
    \left[
      v(x) a(x) u'(x) 
    \right]_0^1  
  \end{align*}
  %
  viz též Matematika~3, např.~\cite{}. Bilineární forma je symetrická.

  \item Zeslabení $C_0^2(0,1) \rightarrow C_0^1(0,1)$ a požadavek na koeficient
  %
  \begin{align}\label{eq:coef_bnd}
    0 < c \leq a(x) \leq C < \infty \text{ pro všechna } x \in (0,1)  
  \end{align}

  \item Skalární součin
  %
  \begin{align*}
    (u, v)_{C_0^1(0, 1)}
    =
    \int_0^1 u'(x) v'(x) \de x
  \end{align*}

  \item Zúplnění vede na Hilbertův prostor
  %
  \begin{align*}
    H_0^1(0, 1)
    =
    \{ 
      v : (0,1) \rightarrow \mathbb{R};
      \int_{0}^{1} (v'^2(x))^2 \de x
      < 
    \infty \}
  \end{align*}

  \item Platí, že 
  %
  \begin{align*}
    H_0^1(0,1) \subset C_0(0,1) \subset L^2(0,1)
  \end{align*}
  %
  kde první inkluze vyplývá z věty o Sobolovevých vnoření, např.~\cite{}, a druhá je důsledkem tzv. Friedrichsovy nerovnosti for libovolné $u \in H_0^1(0, 1)$
  %
  \begin{align}\label{eq:fried}
     \| u \|_{L_2(0,1)} 
    \leq
    \frac{1}{\pi}
    \| u' \|_{L_2(0,1)}
  \end{align}

  \item Bilineární forma je tedy dobře definovaná i pro $v \in H_0^1(0,1)$.

\end{itemize}

\subsection{Řešení okrajových úloh}

\begin{itemize}
  \item Slabé řešení: Najdi $u \in H_0^1(0, 1)$ takové, že 
  %
  \begin{align*}
    \boxed{
    A(u, v ) = b( v ) 
    \text{ pro všechna }
    v \in H_0^1(0, 1)}
  \end{align*}

  \item Variační řešení 
  %
  \begin{align*}
    \boxed{
    u = \arg \min_{v \in H_0^1(0,1)}
    J( v ) = \frac{1}{2} A(v,v) - b(v)
    }
  \end{align*}

\end{itemize}

\clearpage
\section{Kvalitativní vlastnosti variačního řešení}

\subsection{Věta o minumu kvadratického funkcionálu}

\paragraph{Věta.} Buď $V$ Hilbertův prostor, $A : V \times V \rightarrow \mathbb{R}$ bilineární forma, která je 
%
\begin{itemize}\noitemsep
  \item symetrická:
  %
  \begin{align}\label{ass:sym}
    A(u, v) = A(v, u)
    \text{ pro všechna} u, v \in V, 
  \end{align}

  \item eliptická: Existuje $c_1 \in \mathbb{R}_+$ takové, že
  %
  \begin{align}\label{ass:ell}
    A(u, u) \geq c_1 \| u \|_V^2
    \text{ pro všechna } u \in V, 
  \end{align}

  \item omezená: Existuje $c_2 \in \mathbb{R}$ takové, že
  %
  \begin{align}\label{ass:bnd}
    | A(u, v) | \leq c_2 \| u \|_V \| v \|_V
    \text{ pro všechna } u, v \in V, 
  \end{align}

\end{itemize}
%
a $b: V \rightarrow \mathbb{R}$ lineární forma, která je 
%
\begin{itemize}
  \item omezená: Existuje $c_3 \in \mathbb{R}$ takové, že
  %
  \begin{align}\label{ass:bnd2}
    | b( v) | \leq c_3 \| v \|_V
    \text{ pro všechna } v \in V. 
  \end{align}
\end{itemize}
%
Pak má úloha
%
\begin{align}
  u = \arg \min_{v \in V}
  J( v ) = \frac{1}{2} A(v,v) - b(v)
\end{align}
%
právě jedno řešení.

\paragraph{Důkaz} založený na tzv. přímé metodě variačního počtu, rozdělíme do šesti kroků.

\subsubsection{$J$ je zezdola omezený}

\begin{align*}
J(v) 
=
\frac{1}{2} A(v,v) - b(v)
\stackrel{\eqref{ass:ell}}{\geq}
\frac{c_1}{2} \| v \|_V^2 
- 
| b( v ) |
\stackrel{\eqref{ass:bnd}}{\geq}
\frac{c_1}{2} \| v \|_V^2 
- 
c_3 \| v \|_{V} 
\end{align*}	
%
Kvadratický člen vhodně upravíme a dále odhadneme
%
\begin{align*}
\frac{c_1}{2} \| v \|_V^2 - c_3 \| v \|_V 
=
\frac{1}{2 c_1}
\left(
  c_1 \| v \|_V - c_3 
\right)^2
-
\frac{c_3^2}{2 c_1}
\geq 
-
\frac{c_3^2}{2 c_1}
\end{align*}	
%
Platí tedy, že 
%
\begin{align*}
  J(v) 
  \geq 
  -
  \frac{c_3^2}{2 c_1}
  \text{ for }
  v \in V.  
\end{align*}

\subsubsection{$J$ je spojitý}

Uvažujme posloupnost $\{ v_n \}_{n \in \mathbb{N}}$ konvergující k $v \in V$, tj.
%
\begin{align*}
 \lim_{n \rightarrow \infty} \| v_n - v \|_{V} = 0 
\end{align*}
%
Pro rozdíl hodnot funkcionálu pak platí
%
\begin{align*}
  | J(v) - J(v_n) |
  & =
   \left| \frac{1}{2} A(v,v) - b(v) - \bigl(  \frac{1}{2} A(v_n,v_n) - b(v_n) \bigr) \right|
   \leq
   \frac{1}{2}
   | A(v,v) -  A(v_n,v_n) |
   +
   | b( v - v_n ) |
   \\
   & \stackrel{\eqref{ass:bnd2}}{\leq}
   \frac{1}{2}
   | A(v,v) -  A(v_n,v_n) |
   +
   c_3 \| v - v_n \|_{V} 
\end{align*}

Poslední člen upravíme s přihlédnutím k jeho linearitě a symetrii,
%
\begin{align*}
  | A(v,v) -  A(v_n,v_n) |
  & =
  | A(v - v_n, v + v_n )  |
  \stackrel{\eqref{ass:bnd}}{\leq}
  c_2 \| u - v_n \|_{V} \| u + v_n \|_{V}
\end{align*}
%
Celkově  
%
\begin{align*}
  | J(v) - J(v_n) | 
  \leq 
  \left( 
    2 c_2 \| u + v_n \|_{V}
    +
    c_3
  \right)
  \| u - v_n \|_{V}
\end{align*}
%
z čehož plyne
%
\begin{align}\label{eq:spojistost_J}
  \lim_{n \rightarrow \infty} J(v_n) = J(v).
\end{align}

\subsubsection{Minimalizující posloupnost}

Označme infimum hodnot funkcionálu jako $j$, tj. 
%
\begin{align}\label{eq:inf}
j = \inf_{v \in V} J(v) 
\end{align}
%
Minimalizující posloupnosti pak budeme rozumět posloupnost $\{v_n\}_{n \in \mathbb{N}}$, pro kterou platí
% 
\begin{align}\label{eq:min_seq}
  j \leq  J( v_n ) \leq j + \frac{1}{n},
  &&
  \lim_{n \rightarrow \infty} J( v_n ) = j.
\end{align}

\subsubsection{Každá minimalizující posloupnost je Cauchyova}

Tato skutečnost vyplývá přímo z kvadratické struktury $J$. Platí, že
%
\begin{align*}
c_1 \| v_n - v_m \|_V^2
& \stackrel{\eqref{ass:ell}}{\leq}
A( v_n - v_m, v_n - v_m )
\stackrel{\eqref{ass:sym}}{=}
2A( v_n, v_n ) + 2A( v_m, v_m )
-
A( v_n + v_m, v_n + v_m )
\\
& =
2A( v_n, v_n ) + 2A( v_m, v_m )
-
4 A( \frac{v_n + v_m}{2}, \frac{v_n + v_m}{2} ).
\end{align*}
%
Použitím identity $A(v,v) = 2 ( J(v) + b(v) )$ dostáváme, že	
%
\begin{align*}
c_1 \| v_n - v_m \|_V^2
& \leq 
4 ( J(v_n) + b(v_n) ) + 4 ( J(v_m) + b(v_m) ) - 8 \left( J( \frac{v_n+v_m}{2}) + \frac{1}{2} b(v_n) + \frac{1}{2} b(v_m) \right) 
\\
& =
4 J(v_n) + 4 J(v_m) - 8 J ( \frac{v_n + v_m}{2} )
\stackrel{\eqref{eq:inf}}{\leq} 
4 J(v_n) + 4 J(v_m) - 8 j,
\end{align*}	
%
kde jsme využili nerovnosti $j \leq J( v )$ z \eqref{eq:inf}. Protože ale $J(v_n) \rightarrow j$ a $J(v_m) \rightarrow j$ pro $n,m \rightarrow \infty$, platí, že $ \| v_n - v_m \|_v \rightarrow 0$ a posloupnost je Cauchyova.

\subsubsection{Existence}

Úplnost $V$ znamená, že existuje $u \in V$ takové, že 
%
\begin{align*}
\lim_{n \rightarrow } \| v_n - u \|_{V} = 0
\end{align*}	
%
a ze spojitosti funkcionálu dostáváme
%
\begin{align*}
j 
\stackrel{\eqref{eq:inf}}{=} 
\lim_{n \rightarrow \infty} J( v_n ) 
\stackrel{\eqref{eq:spojistost_J}}{=}
J( u ),
\end{align*}	
%
$u$ je tedy hledaným bodem minima.

\subsubsection{Jednoznačnost}

Dokážeme sporem. Nechť $u_1$ a $u_2$ jsou dvě různá řešení úlohy. Pak posloupnost ${u_1, u_2, u_1, u_2, ...}$ je minimalizující dle~\eqref{eq:min_seq}, tedy Cauchyova. Platí tedy, že $\| u_1 - u_2 \|_V = 0$; obě řešení jsou tedy stejná.

\subsection{Aplikace na okrajové úlohy}

\begin{itemize}
  \item Prostor $V = H_0^1(0, 1)$
  \item Elipticita bilineární formy~($c_1 = c$)
  %
  \begin{align*}
    A(u, u) 
    =
    \int_{0}^{1}
    a(x) 
    u'(x)^2 
    \de x   
    \stackrel{\eqref{eq:coef_bnd}}{\geq}
    c
    \int_{0}^{1}
    u'(x)^2 
    \de x   
    =
    c \| u \|_{H_0^1(0, 1)}^2     
  \end{align*}

  \item Omezenost bilineární formy~($c_2 = C$)
  %
  \begin{align*}
    | A(u, v) | 
    =
    |
    \int_{0}^{1}
    v'(x) a(x) u'(x) 
    \de x
    |      
    \stackrel{\eqref{eq:coef_bnd}}{\leq}
    C
    | (u, v)_{H_0^1(0, 1)} |
    \int_{0}^{1}
    \leq 
    C \| u \|_{H_0^1(0, 1)}
    \| v \|_{H_0^1(0, 1)}     
  \end{align*}
  %
  kde poslední krok vyplývá z Cauchyho-Schwartzovy nerovnost na $H_0^1(0, 1)$.

  \item Omezenost lineární formy~($c_3 = \| f \|_{L^2(0, 1)} / \pi$)
  %
  \begin{align*}
    | b(v) |
    =
    | (v,f)_{L^2(0, 1)} |
    \leq
    \| f \|_{L^2(0, 1)}
    \| v \|_{L^2(0, 1)}
    \leq
    \frac{1}{\pi}
    \| f \|_{L^2(0, 1)}
    \| v \|_{H^1_{0}(0, 1)}
  \end{align*}
  %
  Vyplývá z Cauchyho-Schwartzovy nerovnosti na $L^2(0, 1)$ a Friedrichsovy nerovnosti

\end{itemize}

\subsection*{Náměty na domácí úkol}
%
\begin{enumerate}\noitemsep
  \item Pro které funkce $u$ platí rovnost v Friedrichsova nerovnosti~\eqref{eq:fried}?
  
  \item Dokažte, že existuje alespoň jedna minimalizující posloupnost z~\eqref{eq:min_seq}. \\[1ex]
  \emph{Nápověda.} Předpokládejte, že existuje $u_m$, pro které není~\eqref{eq:min_seq} splněno a ukažte, že tento předpoklad vede ke sporu.
  
  \item Dokažte, že tvrzení Věty 3.1 lze doplnit o 
  %
  \begin{quote}
    Navíc toto $u$ je současně i slabým řešením. 
  \end{quote}
  %
  Jako inspiraci využijte důkaz ze Sekce~7 části I tohoto předmětu~\cite{}.

\end{enumerate}

\section{Další informace}

\end{document}