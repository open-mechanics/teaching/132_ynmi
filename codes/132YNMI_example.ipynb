{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "provenance": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "language_info": {
      "name": "python"
    }
  },
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "id": "VXVMMEHtNbsQ",
        "collapsed": true
      },
      "outputs": [],
      "source": [
        "# Import FEniCS system\n",
        "try:\n",
        "    from dolfin import *\n",
        "except ImportError:\n",
        "    !wget \"https://fem-on-colab.github.io/releases/fenics-install-real.sh\" -O \"/tmp/fenics-install.sh\" && bash \"/tmp/fenics-install.sh\"\n",
        "    from dolfin import *\n",
        "\n",
        "# Import matliblot\n",
        "from matplotlib import *"
      ]
    },
    {
      "cell_type": "markdown",
      "source": [
        "This example concerns the 1D elasticity problem:\n",
        "\n",
        "\\begin{align*}\n",
        "-EA u^{\\prime\\prime}(x) & = f_0 ( 1 - x ) \\\\\n",
        "u(0) & = u(1) = 0\n",
        "\\end{align*}\n",
        "\n",
        "with the following choice of parameters: $E = 20$ GPa, $A = 4 \\cdot 10^{-2}$ m$^{2}$, and $f_0 = 2$ MNm$^{-1}$. The exact solution to this problem reads as\n",
        "\n",
        "\\begin{align*}\n",
        "u( x ) = c\n",
        "\\left(\n",
        "  \\frac{x^3}{6}\n",
        "  -\n",
        "  \\frac{x^2}{2}\n",
        "  +\n",
        "  \\frac{x}{3}\n",
        "\\right),\n",
        "&&\n",
        "c = \\frac{f_0}{EA}\n",
        "\\end{align*}"
      ],
      "metadata": {
        "id": "AuwDV9tKc19J"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "# Problem parameters (in basic units)\n",
        "\n",
        "E = 20e9 # Pa\n",
        "A = 4e-2 # m^2\n",
        "f_0 = 2e6 # Nm^{-1}\n",
        "\n",
        "# Auxiliary functions needed later\n",
        "EA = Constant(E*A) # EA is a constant function of x[ 0 ]\n",
        "f  = Expression ( \"_f_0 * (1 - x[0])\", _f_0 = f_0, degree = 1 ) # Right-hand side\n",
        "u_ex = Expression( \"_c * (1./6 * pow(x[0], 3) - 1./2 * pow(x[0], 2) + 1./3 * x[0])\",\n",
        "                    _c = f_0 / (E*A), degree = 3 ) # Exact solution\n",
        "\n",
        "# Helper function to implement the boundary conditions\n",
        "def u0_boundary ( x, on_boundary ):\n",
        "    return on_boundary"
      ],
      "metadata": {
        "id": "QjE9kKPLPGte"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "source": [
        "We now need to set the FE mesh on the unit interval for the element size $h$ and build the approximation space for trial and test functions $V_h$, including the enforcement of the essential (or Dirichlet) boundary conditions."
      ],
      "metadata": {
        "id": "PF9QGksEi_qe"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "h = .5 # FE mesh size in [m]\n",
        "\n",
        "mesh = UnitIntervalMesh ( int(1/h) ) # FE mesh\n",
        "\n",
        "V_h = FunctionSpace ( mesh, \"Lagrange\", 3 ) # FE space, with the last argument denoting the polynomial order\n",
        "\n",
        "bc = DirichletBC ( V_h, u_ex, u0_boundary ) # Enforce the boundary conditions"
      ],
      "metadata": {
        "id": "UfViAbonOoVt"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "source": [
        "Recall that the bilinear and linear forms for this problem are given by\n",
        "\n",
        "\\begin{align*}\n",
        "A( u, v ) =\n",
        "\\int_0^1 EA(x) u^\\prime(x) v^\\prime( x ) \\mathrm{d} x,\n",
        "&&\n",
        "b( v ) =\n",
        "\\int_0^1 v( x ) f( x ) \\mathrm{d} x.\n",
        "\\end{align*}"
      ],
      "metadata": {
        "id": "2Muk-VJZkQIM"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "u = TestFunction ( V_h ) # to search for solution\n",
        "v = TrialFunction ( V_h ) # to build the forms\n",
        "\n",
        "Auv = ( inner ( EA * nabla_grad ( u ), nabla_grad ( v ) ) ) * dx\n",
        "bv = v * f * dx"
      ],
      "metadata": {
        "id": "NekpOYEER1I7"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "source": [
        "The approximate solution $u_h \\in V_h$ now follows from the Galerkin method\n",
        "\n",
        "\\begin{align*}\n",
        "A(u_h, v) = b( v )\n",
        "\\text{ for all }\n",
        "v \\in V_h\n",
        "\\end{align*}"
      ],
      "metadata": {
        "id": "yZeZIM1zmIvl"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "u_h = Function( V_h )\n",
        "\n",
        "solve ( Auv == bv, u_h, bc )"
      ],
      "metadata": {
        "id": "u7GrMtxHSdIL"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "source": [
        "Just for plotting purposes, we will build on auxiliary space $V_\\mathrm{plt}$ with the spatial resolution of $0.001$ m and interpolate all functions to plot on this fine grid.\n",
        "\n",
        "\n",
        "Note that this hack is only because my limited FENiCS / Python knowledge and would never be done in serious simulations!"
      ],
      "metadata": {
        "id": "P_QPGBKWswvF"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "mesh_plt = UnitIntervalMesh( 1000 )\n",
        "V_plt = FunctionSpace ( mesh_plt, \"Lagrange\", 1 )"
      ],
      "metadata": {
        "id": "CwMHnYy7UZ3S"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "source": [
        "plot ( interpolate( u_h, V_plt ), title = 'approximate solution u_' + str( h ) )\n",
        "pyplot.savefig ( 'approximate_solution.png' )"
      ],
      "metadata": {
        "id": "k7CbwKl5t_a7"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "source": [
        "The error of the approximate solution is defined as $e_h = u_\\mathrm{ex} - u_h$. We will visualize it first."
      ],
      "metadata": {
        "id": "ufJLh7BRnkwF"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "e_h = interpolate( u_ex, V_plt ) - interpolate( u_h, V_plt )\n",
        "\n",
        "plot( e_h, title = 'error e_' + str( h ) )"
      ],
      "metadata": {
        "id": "Sv4EQhM6XQYM"
      },
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "source": [
        "Finally, we will measure the error in the $L^2(0, 1)$ and $H^1(0, 1)$ norms, i.e. $\\| e_h \\|_{L^2}$ and $\\| e_h \\|_{H^1}$"
      ],
      "metadata": {
        "id": "SbbSaogZsHhy"
      }
    },
    {
      "cell_type": "code",
      "source": [
        "error_L2 = errornorm(u_ex, u_h, 'L2')\n",
        "error_H1 = errornorm(u_ex, u_h, 'H1')\n",
        "\n",
        "print( 'L2 norm of error = ', error_L2, ' ( for h = ', h, ')')\n",
        "print( 'H1 norm of error = ', error_H1, ' ( for h = ', h, ')')"
      ],
      "metadata": {
        "id": "MhwvKqm7Ycn3",
        "collapsed": true
      },
      "execution_count": null,
      "outputs": []
    }
  ]
}