\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{YNMI_prednasky}[Sablona pro materialy k predmetu YNMI]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[czech]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{CisloPrednasky}

\renewcommand{\maketitle}[2]{%
   \setcounter{CisloPrednasky}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Numerické metody v inženýrských úlohách - 132YNMI | Metoda konečných prvků očima "matematika": Přednáška~IV.\theCisloPrednasky: #2}%
    \rfoot{\thepage}%
}

\newcommand{\Reference}{%
\bibliography{liter}
}
